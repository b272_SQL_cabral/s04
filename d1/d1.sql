INSERT INTO artists (name) VALUES ("Taylor Swift"), ("Lady Gaga"), ("Justine Bieber"), ("Ariana Grande"), ("Bruno Mars");

-- Taylor Swift
INSERT INTO albums (album_title, date_released, artists_id) VALUES ("Fearless", "2008-1-1", 3), ("Red", "2012-1-1", 3)

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fearless", 246, "Pop Rock", 3), ("State of Grace", 213, "Rock", 3)
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Love Story", 312, "Country Rock", 4), ("Begin Again", 304, "Country", 4)

-- Lady Gaga
INSERT INTO albums (album_title, date_released, artists_id) VALUES ("A star is Born", "2018-1-1", 4), ("Born This Way", "2011-1-1", 4)

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallow", 250, "Country, rock, folk rock", 5), ("Always remember us this way", 310, "Country", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Bad Romance", 320, "Electro, pop", 6), ("Paparazzi", 258, "Pop", 6);

-- Justine Bieber
INSERT INTO albums (album_title, date_released, artists_id) VALUES ("Purpose", "2015-1-1", 5), ("Believe", "2012-1-1", 5)

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", 242, "Country", 7), ("Boyfriend", 320, "Pop", 8);

-- Ariana Grande
INSERT INTO albums (album_title, date_released, artists_id) VALUES ("Dangerous Woman", "2016-1-1", 6), ("Thank U, Next", "2019-1-1", 6)

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Into you", 320, "EDM house", 9), ("Thank U, Next", 246, "Pop, R&B", 10);

-- Bruno Mars
INSERT INTO albums (album_title, date_released, artists_id) VALUES ("24k Magic", "2016-1-1", 7), ("Earth to Mars", "2011-1-1", 7)

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("24k Magic", 400, "Funk, Disco, R&B", 11), ("Lost", 250, "Pop", 12);





